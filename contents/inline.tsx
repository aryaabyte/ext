import "firebase/firestore";
import "firebase/auth";



import { addDoc, collection } from "firebase/firestore";
import { onSnapshot, query } from "firebase/firestore";
import type { PlasmoContentScript, PlasmoGetInlineAnchor } from "plasmo";
import { useEffect, useState } from "react";



import { useStorage } from "@plasmohq/storage/hook";



import { db } from "../firebase";





export const config: PlasmoContentScript = {
  matches: ["https://www.linkedin.com/*"]
}

export const getInlineAnchor: PlasmoGetInlineAnchor = () =>
  document.querySelector(".pv-top-card-v2-ctas .pvs-profile-actions ")

export const getShadowHostId = () => "h2"
const Inline = () => {
  const [user] = useStorage("userData")

  if (user) {
    console.log(user)
  } else {
    console.log("No user is currently logged in")
  }
  const [profileDetails, setProfileDetails] = useState({
    fullname: "",
    headline: "",
    experience: "",
    volunteering: "",
    currentCompany: "",
    previousCompany: ""
  })

  const handleSaveProfile = async (e) => {
    e.preventDefault()
    if (!user) {
      alert("Please log in to save the profile.")
      return
    }

    const existingProfile = profile.find(
      (todo) =>
        todo.userId === user.uid && todo.fullname === profileDetails.fullname
    )
    if (existingProfile) {
      alert("Profile already exists!")
      return
    }

    try {
      await addDoc(collection(db, "profile"), {
        userId: user.uid,
        fullname: profileDetails.fullname,
        headline: profileDetails.headline,
        experience: profileDetails.experience,
        volunteering: profileDetails.volunteering,
        currentCompany: profileDetails.currentCompany,
        previousCompany: profileDetails.previousCompany
        // createdAt: firebase.firestore().FieldValue.serverTimestamp()
      })
      alert("Profile saved successfully!")
    } catch (error) {
      console.error("Error saving profile: ", error)
      alert("Error saving profile, please try again later.")
    }
  }

  var exp

  var vol

  var curComp

  var prevComp
  const [profile, setprofile] = useState([])
  useEffect(() => {
    const q = query(collection(db, "profile"))
    const unsub = onSnapshot(q, (querySnapshot) => {
      let profileArray = []
      querySnapshot.forEach((doc) => {
        profileArray.push({ ...doc.data(), id: doc.id })
      })
      console.log(user.displayName)
      const filteredData = profileArray.filter(
        (data) => data.userId === user.uid
      )
      setprofile(filteredData)
      // setprofile(profileArray);
    })
    return () => unsub()
  }, [user])
  console.log(profile)

  useEffect(() => {
    setTimeout(() => {
      setProfileDetails({
        fullname: document.querySelector(".pv-top-card .text-heading-xlarge")
          .textContent,
        headline: document.querySelector(".pv-top-card .text-body-medium")
          .textContent
          ? document.querySelector(".pv-top-card .text-body-medium").textContent
          : "no headline",
        experience:
          ((exp =
            document === null || document === void 0
              ? void 0
              : document.querySelector("#experience")) === null ||
          exp === void 0
            ? void 0
            : exp.parentElement.querySelector(
                " div.pvs-list__outer-container > ul"
              ).innerHTML) || "",
        volunteering:
          (vol =
            document === null || document === void 0
              ? "no volunteering"
              : document.querySelector("#volunteering_experience")) === null ||
          vol === void 0
            ? "no volunteering"
            : vol.parentElement.querySelector(
                " div.pvs-list__outer-container > ul"
              ).innerHTML || "no volunteering",
        currentCompany:
          (curComp =
            document === null || document === void 0
              ? "unemployed"
              : document.querySelector("#experience")) === null ||
          curComp === void 0
            ? "unemployed"
            : curComp.parentElement.querySelector(
                "div.pvs-list__outer-container > ul > li:nth-child(1) > div > div.display-flex.flex-column.full-width.align-self-center > div > div.display-flex.flex-column.full-width > span:nth-child(2) > span:nth-child(1)"
              ) === null
            ? curComp.parentElement.querySelector(
                "div.pvs-list__outer-container > ul > li:nth-child(1) > div > div.display-flex.flex-column.full-width.align-self-center > div.display-flex.flex-row.justify-space-between > a > div > span > span:nth-child(1)"
              ).innerHTML
            : curComp.parentElement.querySelector(
                "div.pvs-list__outer-container > ul > li:nth-child(1) > div > div.display-flex.flex-column.full-width.align-self-center > div > div.display-flex.flex-column.full-width > span:nth-child(2) > span:nth-child(1)"
              ).innerHTML,
        previousCompany:
          (prevComp =
            document === null || document === void 0
              ? "no previous company"
              : document.querySelector("#experience")) === null ||
          prevComp === void 0
            ? "no previous company"
            : prevComp.parentElement.querySelector(
                " div.pvs-list__outer-container > ul > li:nth-child(2) > div > div.display-flex.flex-column.full-width.align-self-center > div.display-flex.flex-row.justify-space-between > div.display-flex.flex-column.full-width > span:nth-child(2) > span:nth-child(1)"
              ) === null
            ? prevComp.parentElement.querySelector(
                "div.pvs-list__outer-container > ul > li:nth-child(2) > div > div.display-flex.flex-column.full-width.align-self-center > div.display-flex.flex-row.justify-space-between > a > div > span > span:nth-child(1)"
              ) === null
              ? "no previous company"
              : prevComp.parentElement.querySelector(
                  "div.pvs-list__outer-container > ul > li:nth-child(2) > div > div.display-flex.flex-column.full-width.align-self-center > div.display-flex.flex-row.justify-space-between > a > div > span > span:nth-child(1)"
                ).innerHTML
            : prevComp.parentElement.querySelector(
                " div.pvs-list__outer-container > ul > li:nth-child(2) > div > div.display-flex.flex-column.full-width.align-self-center > div.display-flex.flex-row.justify-space-between > div.display-flex.flex-column.full-width > span:nth-child(2) > span:nth-child(1)"
              ).innerHTML
      })
    }, 3000)
  }, [])
  console.log(profileDetails)

  return user ? (
    user.displayName === document.querySelector(".pv-top-card .text-heading-xlarge")
          .textContent ? (
      ""
    ) : (
      <button
        style={{
          alignItems: "center",
          backgroundColor: "#0A66C2",
          borderRadius: "100px",
          color: "#ffffff",
          fontSize: "16px",
          fontWeight: 600,
          padding: "8px",
          overflow: "hidden",
          fontFamily:
            '-apple-system, system-ui, system-ui, "Segoe UI", Roboto, "Helvetica Neue", "Fira Sans", Ubuntu, Oxygen, "Oxygen Sans", Cantarell, "Droid Sans", "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Lucida Grande", Helvetica, Arial, sans-serif',
          boxShadow: "none"
        }}
        onClick={handleSaveProfile}>
        Save profile
      </button>
    )
  ) : (
    ""
  )
}

export default Inline

// profileDetails.currentCompany,
//         profileDetails.experience,
//         profileDetails.fullname,
//         profileDetails.headline